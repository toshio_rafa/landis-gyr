﻿using Consumption_API.Services.meter;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consumption_API.Dep_Inj_Config.services
{
    public static class CoreServices
    {
        public static IServiceCollection StartupExtensions(this IServiceCollection services)
        {
            #region Interfaces Implementation Repository
            #endregion

            #region Interfaces Implementation Services
            #endregion          

            return services;
        }
    }
}
