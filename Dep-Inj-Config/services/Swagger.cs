﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consumption_API.Dep_Inj_Config.services
{
    public static class Swagger
    {
        public static IServiceCollection SwaggerStartupExtensions(this IServiceCollection services)
        {            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API Landis+Gyr", Version = "v1", Description = "This is an API for Landis+Gyr" });
            });
            return services;
        }
    }
}
