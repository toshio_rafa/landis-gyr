﻿using Consumption_API.Dto;
using Consumption_API.Models;
using Consumption_API.Services.meter;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Consumption_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsumptionController : ControllerBase
    {
        static readonly IMeter _meter = new MeterService();
        
        [HttpGet]
        public IActionResult GetConsumption(string meter_number)
        {
            try
            {
                var meter = _meter.GetMeter(meter_number);
                return Ok(meter.Consumption);
            }
            catch (Exception)
            {
                return StatusCode(404, 0);
            }
        }

        [HttpGet("microgeneration")]
        public IActionResult GetMicroGeneration(string meter_number)
        {            
            try
            {
                var meter = _meter.GetMeter(meter_number);
                return Ok(meter.Microgeneration);
            }
            catch (Exception)
            {
                return StatusCode(404, 0);
            }
        }


        [HttpPost("event")]
        public IActionResult Post([FromBody] MeterDto meterDto)
        {
            try
            {
                var meter = _meter.CreateMeter(meterDto);
                return StatusCode(201, meter);
            }
            catch (Exception)
            {
                return StatusCode(404, 0);
            }
            
        }      
    }
}