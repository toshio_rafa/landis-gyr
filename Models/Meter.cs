﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consumption_API.Models
{
    public class Meter
    {
        public string MeterNo { get; set; }
        public int Consumption { get; set; }
        public int? Microgeneration { get; set; }        
        public double cash { get; set; }
    }
}
