﻿using Consumption_API.Dto;
using Consumption_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consumption_API.Services.meter
{
    public interface IMeter
    {
        Meter GetMeter(string meterNumber);      
        Meter CreateMeter(MeterDto meter);
    }
}
