﻿using Consumption_API.Dto;
using Consumption_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consumption_API.Services.meter
{
    public class MeterService : IMeter
    {
        public List<Meter> meters = new List<Meter>();


        //-- implementation methods

        public Meter CreateMeter(MeterDto meterDto)
        {
            if (meterDto.Type.Equals("import"))
            {
                return ImportMeter(meterDto);

            }
            else if (meterDto.Type.Equals("push"))
            {
                return PushMeter(meterDto);
            }
            else
            {
                return BillEnergy(meterDto);
            }
            
        }

        public Meter GetMeter(string meterNumber)
        {
            return FindResourceByMeterNumber(meterNumber);
        }


        //-- private methods

        private Meter ImportMeter(MeterDto meterDto)
        {                        
            var meter = new Meter
            {
                MeterNo = meterDto.MeterNo,
                Consumption = meterDto.ActiveEnergy,
                Microgeneration = meterDto.InjectedEnergy,
            };
            meters.Add(meter);
            return meter;
        }

        private Meter PushMeter(MeterDto meterDto)
        {
            var resource = FindResourceByMeterNumber(meterDto.MeterNo);            
            resource.Consumption = meterDto.ActiveEnergy;
            resource.Microgeneration = meterDto.InjectedEnergy;
            return resource;
        }

        private Meter BillEnergy(MeterDto meterDto)
        {
            var resource = FindResourceByMeterNumber(meterDto.MeterNo);          
            resource.cash = GetMeterBilling(resource, meterDto.unit);
            return resource;
        }

        private Meter FindResourceByMeterNumber(string meterNumber)
        {
            var resource = meters.Find(x => x.MeterNo.Equals(meterNumber));
            if (resource == null) throw new Exception();
            return resource;
        }

        private double GetMeterBilling(Meter meter, double unit)
        {
            return ((double)(unit * (meter.Consumption - meter.Microgeneration)));
        }

    }
}
