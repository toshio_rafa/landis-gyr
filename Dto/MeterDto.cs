﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consumption_API.Dto
{
    public class MeterDto
    {
        public string MeterNo { get; set; }
        public int ActiveEnergy { get; set; }
        public int InjectedEnergy { get; set; }
        public string Type { get; set; }
        public double unit { get; set; }
    }
}
